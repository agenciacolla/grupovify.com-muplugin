<?php
/**
 * Grupo Vify - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   Grupo Vify
 * @author    Black Magenta <contato@blackmagenta.com.br>
 * @copyright 2022 Black Magenta
 * @license   Proprietary https://blackmagenta.com.br
 * @link      https://blackmagenta.com.br
 *
 * @wordpress-plugin
 * Plugin Name: Grupo Vify - mu-plugin
 * Plugin URI:  https://blackmagenta.com.br
 * Description: Customizations for grupovify.com site
 * Version:     1.0.0
 * Author:      Black Magenta
 * Author URI:  https://blackmagenta.com.br/
 * Text Domain: grupovifycom
 * License:     Proprietary
 * License URI: https://blackmagenta.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('grupovifycom', basename(dirname(__FILE__)).'/languages');
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

/**********
 * Show on Front Page
 *
 * @return bool $display
 */
function Show_On_Front_page($cmb)
{
    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option('page_on_front');

    // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($front_page) : $front_page;

    // There is a front page set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/**********
 * Show on Privacy Policy 
 *
 * @return bool $display
 */
function Show_On_Privacy($cmb)
{
    // Get ID of page set as privacy, 0 if there isn't one
    $privacy = get_option('wp_page_for_privacy_policy');

     // Get the translated page of the curent language
    $translated_page = function_exists('pll_get_post') ? pll_get_post($privacy) : $privacy;

    // There is a privacy set  - are we editing it?
    return $cmb->object_id() == $translated_page;
}

/***********************************************************************************
 * Page Fields
 * ********************************************************************************/
/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        $prefix = '_grupovify_frontpage_';


        /************
         * Vify Training
         */
        $cmb_service_training = new_cmb2_box(
            array(
                'id'            => '_grupovify_frontpage_service_training_id',
                'title'         => __('Vify Training', 'grupovifycom'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Title
        $cmb_service_training->add_field(
            array(
                'name'       => __('Title', 'grupovifycom'),
                'desc'       => '',
                'id'         => $prefix . 'service_training_title',
                'type'       => 'text',
            )
        );

        //Featured Image
        $cmb_service_training->add_field(
            array(
                'name'    => __('Featured Image', 'grupovifycom'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1140x556px | Aspect ratio: 41:20', 'grupovifycom'),
                'id'      => $prefix .'service_training_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'grupovifycom'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(328, 160) 
            )
        );

        //Description
        $cmb_service_training->add_field(
            array(
                'name'       => __('Description', 'grupovifycom'),
                'desc'       => '',
                'id'         => $prefix . 'service_training_desc',
                'type'       => 'textarea_code',
            )
        );

        //Services Group
        $services_id = $cmb_service_training->add_field(
            array(
                'id'          => $prefix . 'service_training_services',
                'type'        => 'group',
                'description' => __('Services', 'grupovifycom'),
                'options'     => array(
                    'group_title'   => __('Service {#}', 'grupovifycom'),
                    'add_button'    => __('Add Another Service', 'grupovifycom'),
                    'remove_button' => __('Remove Service', 'grupovifycom'),
                    'sortable'      => true,
                ),
            )
        );

        //Service Name
        $cmb_service_training->add_group_field(
            $services_id,
            array (
                'name'       => __('Name', 'grupovifycom'),
                'desc'       => '',
                'id'         => 'name',
                'type'       => 'text',
            )
        );

        //Service Button Text
        $cmb_service_training->add_group_field(
            $services_id,
            array(
                'name'       => __('Button Text', 'grupovifycom'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text',
            )
        );

        //Service Button URL
        $cmb_service_training->add_group_field(
            $services_id,
            array(
                'name'       => __('Button Link (URL)', 'grupovifycom'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );

        //Themes
        $cmb_service_training->add_field(
            array(
                'name'       => __('Themes', 'grupovifycom'),
                'desc'       => __('Themes:', 'grupovifycom'),
                'id'         => $prefix . 'service_training_themes',
                'type'       => 'textarea_small',
            )
        );

        /************
         * Vify Insurance
         */
        $cmb_service_insurance = new_cmb2_box(
            array(
                'id'            => '_grupovify_frontpage_service_insurance_id',
                'title'         => __('Vify Insurance', 'grupovifycom'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Title
        $cmb_service_insurance->add_field(
            array(
                'name'       => __('Title', 'grupovifycom'),
                'desc'       => '',
                'id'         => $prefix . 'service_insurance_title',
                'type'       => 'text',
            )
        );

        //Featured Image
        $cmb_service_insurance->add_field(
            array(
                'name'    => __('Featured Image', 'grupovifycom'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1140x556px | Aspect ratio: 41:20', 'grupovifycom'),
                'id'      => $prefix .'service_insurance_img',
                'type'    => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add Image', 'grupovifycom'), 
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                        //'image/svg+xml',
                    ),
                ),
                'preview_size' => array(328, 160) 
            )
        );

        //Description
        $cmb_service_insurance->add_field(
            array(
                'name'       => __('Description', 'grupovifycom'),
                'desc'       => '',
                'id'         => $prefix . 'service_insurance_desc',
                'type'       => 'textarea_code',
            )
        );

        //Services Group
        $services_id = $cmb_service_insurance->add_field(
            array(
                'id'          => $prefix . 'service_insurance_services',
                'type'        => 'group',
                'description' => __('Services', 'grupovifycom'),
                'options'     => array(
                    'group_title'   => __('Service {#}', 'grupovifycom'),
                    'add_button'    => __('Add Another Service', 'grupovifycom'),
                    'remove_button' => __('Remove Service', 'grupovifycom'),
                    'sortable'      => true,
                ),
            )
        );

        //Service Name
        $cmb_service_insurance->add_group_field(
            $services_id,
            array (
                'name'       => __('Name', 'grupovifycom'),
                'desc'       => '',
                'id'         => 'name',
                'type'       => 'text',
            )
        );

        //Service Button Text
        $cmb_service_insurance->add_group_field(
            $services_id,
            array(
                'name'       => __('Button Text', 'grupovifycom'),
                'desc'       => '',
                'id'         => 'btn_text',
                'type'       => 'text',
            )
        );

        //Service Button URL
        $cmb_service_insurance->add_group_field(
            $services_id,
            array(
                'name'       => __('Button Link (URL)', 'grupovifycom'),
                'desc'       => '',
                'id'         => 'btn_url',
                'type'       => 'text',
            )
        );

        /************
         * Our Experts
         */
        $cmb_experts = new_cmb2_box(
            array(
                'id'            => '_grupovify_frontpage_experts_id',
                'title'         => __('Our Experts', 'grupovifycom'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Experts Group
        $experts_id = $cmb_experts->add_field(
            array(
                'id'          => $prefix . 'experts',
                'type'        => 'group',
                'description' => __('Experts', 'grupovifycom'),
                'options'     => array(
                    'group_title'   => __('Expert {#}', 'grupovifycom'),
                    'add_button'    => __('Add Another Expert', 'grupovifycom'),
                    'remove_button' => __('Remove Expert', 'grupovifycom'),
                    'sortable'      => true,
                ),
            )
        );

        //Expert Photo
        $cmb_experts->add_group_field(
            $experts_id,
            array(
                'name'        => __('Photo', 'grupovifycom'),
                'desc'    => __('Format: JPG (recommended), PNG | Recommended size: 1080x880px | Aspect ratio: 27:22', 'grupovifycom'),
                'id'          => 'img',
                'type'        => 'file',
                'options' => array(
                    'url' => false, 
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'grupovifycom'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(270, 220)
            )
        );

        //Expert Name
        $cmb_experts->add_group_field(
            $experts_id,
            array (
                'name'       => __('Name', 'grupovifycom'),
                'desc'       => '',
                'id'         => 'name',
                'type'       => 'text',
            )
        );

        //Expert Description
        $cmb_experts->add_group_field(
            $experts_id,
            array (
                'name'       => __('Description', 'grupovifycom'),
                'desc'       => '',
                'id'         => 'desc',
                'type'       => 'textarea_small',
            )
        );

        /**
        * Form
        */
        $cmb_form = new_cmb2_box(
            array(
                'id'            => '_grupovify_frontpage_form_id',
                'title'         => __('Form', 'grupovifycom'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Shortcode
        $cmb_form->add_field( 
            array(
                'name'       => __('Form Shortcode', 'grupovifycom'),
                'desc'       => 'Contact Form 7 Shortcode',
                'id'         => $prefix . 'form_shortcode',
                'type'       => 'text',
            )
        );

        /**
        * Testimonials
        */
        $cmb_testimonials = new_cmb2_box(
            array(
                'id'            => '_grupovify_frontpage_testimonials_id',
                'title'         => __('Testimonials', 'grupovifycom'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Testimonials Group
        $testimonial_id = $cmb_testimonials->add_field(
            array(
                'id'          => $prefix . 'testimonials',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   => __('Testimonial {#}', 'grupovifycom'),
                    'add_button'    => __('Add Another Testimonial', 'grupovifycom'),
                    'remove_button' => __('Remove Testimonial', 'grupovifycom'),
                    'sortable'      => true,
                ),
            )
        );

        // Testimonial Name
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array (
                'name'       => __('Name', 'grupovifycom'),
                'desc'       => '',
                'id'         => 'name',
                'type'       => 'text',
            )
        );

        // Testimonial Description
        $cmb_testimonials->add_group_field(
            $testimonial_id,
            array (
                'name'       => __('Testimonial', 'grupovifycom'),
                'desc'       => '',
                'id'         => 'content',
                'type'       => 'textarea',
            )
        );

        
        /**
        * Contact
        */
        $cmb_contact = new_cmb2_box(
            array(
                'id'            => '_grupovify_frontpage_contact_id',
                'title'         => __('Contact', 'grupovifycom'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Phone
        $cmb_contact->add_field(
            array(
                'name'       => __('Phone', 'grupovifycom'),
                'desc'       => '',
                'id'         => $prefix . 'contact_phone',
                'type'       => 'text_medium',
            )
        );

        //Email
        $cmb_contact->add_field(
            array(
                'name'       => __('Email', 'grupovifycom'),
                'desc'       => '',
                'id'         => $prefix . 'contact_email',
                'type'       => 'text_email',
            )
        );

        //WhatsApp
        $cmb_contact->add_field(
            array(
                'name'       => __('WhatsApp (URL)', 'grupovifycom'),
                'desc'       => '',
                'id'         => $prefix . 'contact_whatsapp_url',
                'type'       => 'text',
            )
        );

        /**
        * Social Networks
        */
        $cmb_social = new_cmb2_box(
            array(
                'id'            => '_grupovify_frontpage_social_id',
                'title'         => __('Social Networks', 'grupovifycom'),
                'object_types'  => array('page'), // Post type
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, 
                'show_on_cb' => 'Show_On_Front_page',
            )
        );

        //Facebook (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('Facebook (URL)', 'grupovifycom'),
                'desc'       => '',
                'id'         => $prefix . 'social_facebook_url',
                'type'       => 'text_url',
            )
        );

        //Instagram (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('Instagram (URL)', 'grupovifycom'),
                'desc'       => '',
                'id'         => $prefix . 'social_instagram_url',
                'type'       => 'text_url',
            )
        );

        //LinkedIn (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('LinkedIn (URL)', 'grupovifycom'),
                'desc'       => '',
                'id'         => $prefix . 'social_linkedin_url',
                'type'       => 'text_url',
            )
        );

        //Youtube (URL)
        $cmb_social->add_field(
            array(
                'name'       => __('Youtube (URL)', 'grupovifycom'),
                'desc'       => '',
                'id'         => $prefix . 'social_youtube_url',
                'type'       => 'text_url',
            )
        );
    }
);
