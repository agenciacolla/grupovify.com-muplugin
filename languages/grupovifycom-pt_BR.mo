��    (      \  5   �      p     q     �     �  	   �     �     �     �     �     �  
   �     �               #     (  S   7  S   �     �     �     �                         )     8     K     W     `     p     |     �     �     �     �     �     �     �     �  e  �     N     j     �     �     �     �     �     �     �     �     �               .     :  V   S  V   �     	     	      	     %	     :	     C	     H	     ]	     n	     �	  	   �	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	      
     
              
      !             	   %   "              (      #      &                                                 '                       $                                       Add Another Expert Add Another Service Add Another Testimonial Add Image Button Link (URL) Button Text Contact Description Email Expert {#} Experts Facebook (URL) Featured Image Form Form Shortcode Format: JPG (recommended), PNG | Recommended size: 1080x880px | Aspect ratio: 27:22 Format: JPG (recommended), PNG | Recommended size: 1140x556px | Aspect ratio: 41:20 Instagram (URL) LinkedIn (URL) Name Our Experts Phone Photo Remove Expert Remove Service Remove Testimonial Service {#} Services Social Networks Testimonial Testimonial {#} Testimonials Themes Themes: Title Vify Insurance Vify Training WhatsApp (URL) Youtube (URL) Project-Id-Version: 
PO-Revision-Date: 2022-04-16 13:43-0300
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: grupovifycom.php
 Adicionar Novo Especialista Adicionar Novo Serviço Adicionar Novo Depoimento Adicionar imagem Link do Botão (URL) Texto do Botão Contato Descrição E-mail Especialista {#} Expecialistas Facebook (URL) Imagem em destaque Formulário Shortcode do Formulário Formato: JPG (recomendado), PNG | Tamanho recomendado: 1080x880px | Proporção: 27:22 Formato: JPG (recomendado), PNG | Tamanho recomendado: 1140x556px | Proporção: 41:20 Instagram (URL) LinkedIn (URL) Nome Nossos Especialistas Telefone Foto Remover Especialista Remover Serviço Remover Depoimento Serviço {#} Serviços Redes Sociais Depoimento Depoimento {#} Depoimentos Temas Temas: Título Vify Seguros Vify Treinamentos WhatsApp (URL) Youtube (URL) 